<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class FollowsController extends Controller
{
    public function store(User $user)
    {
        //1-
        //The authenticated user follow the given user
//        auth()->user()->follow($user);

        //2-
//        if(auth()->user()->following($user)){
//            auth()->user()->unfollow($user);
//        }else {
//            auth()->user()->follow($user);
//        }

        //3-
        auth()->user()->toggleFollow($user);
        return back();
    }
}
