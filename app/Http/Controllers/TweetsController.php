<?php

namespace App\Http\Controllers;

use App\Tweet;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;

class TweetsController extends Controller
{
    public function index()
    {
        return view('tweets.index', [
            'tweets' => auth()->user()->timeline(),
        ]);
    }

    public function store()
    {
        $validated = request()->validate([
           'body' => 'required|max:255'
        ]);

//
        Tweet::create([
            'user_id' => auth()->id(),
            'body' => $validated['body'],
        ]);

        return redirect(route('home'));
    }
}
