<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Validation\Rule;

class ProfilesController extends Controller
{
    public function show(User $user)
    {
        return view('profiles.show', [
            'user' => $user,
            'tweets'=> $user->tweets()->withLikes()->paginate(5),
        ]);
    }

    public function edit(User $user)
    {
        //We Can use it this way without policy
//        if(current_user()->isNot($user)){
//            abort(404);
//        }
//        abort_if(current_user()->isNot($user),404); //same result


        //or this way with policy
//        $this->authorize('edit', $user); // like this or go to routes file add middleware('can:edit')
        return view('profiles.edit', compact('user'));
    }

    public function update(User $user)
    {
        $attributes = request()->validate([
            'username' => ['required', 'string', 'max:255', 'alpha_dash', Rule::unique('users')->ignore($user)],
            'name' => ['required', 'string', 'max:255'],
            'avatar' => ['file'],
            'email' => ['email', 'required', Rule::unique('users')->ignore($user)],
            'password' => ['required', 'string', 'min:8', 'max:255', 'confirmed'],
        ]);

        if (request('avatar')) {
            $attributes['avatar'] = request('avatar')->store('avatars');
        }

        $user->update($attributes);
//        $user->update(request()->all());
        return redirect($user->path());
    }
}
