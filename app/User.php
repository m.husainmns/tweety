<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    use Notifiable, Followable;


    protected $fillable = [
        'username', 'name', 'avatar', 'email', 'password',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function timeline()
    {
//        $ids = $this->follows()->pluck('id');//grab all the ids of users that this uer follows
//        $ids->push( $this->id); // push his id to the collection too
//        return Tweet::whereIn('user_id', $ids)->latest()->get();

        $friends = $this->follows()->pluck('id'); //This is a cleaner code
        return Tweet::whereIn('user_id', $friends)
            ->orWhere('user_id', $this->id)
            ->withLikes()
            ->latest()->paginate(50);
    }

    public function tweets()
    {
        return $this->hasMany(Tweet::class);
    }


    /*
     * This method is to create something called "accessor"
     * so we can use it like this: user->avatar
     */
    public function getAvatarAttribute($value)
    {
//        $picture = Storage::url($value);
        if (!$value) {
            return asset('images/default-avatar.jpeg');
        }
        return asset('storage/' . $value);
//        return "https://avatars.dicebear.com/api/avataaars/" . $this->name . ".svg";
    }

    /*
     * This is a "mutator"
     * $user->password = "foobar"
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }


    public function path($append = '')
    {
        $path = route('profile', $this->username);
        return $append ? "{$path}/{$append}" : $path;
    }

    public function likes(){
        return $this->hasMany(Like::class);
    }

}
