<?php


namespace App;


trait Followable
{

    /*
     * Here we explicitly mention the table because
     * we didn't follow conventions
     */
    public function follows()
    {
        return $this->belongsToMany(User::class,
            'follows',
            'user_id',
            'following_user_id'
        );
    }

    /*
 * include all of the user's tweets
 * as well as the tweets of every one
 * the follow.. in descending order by date
 *
 */
    public function follow(User $user)
    {
        //this user wants to follow a user so,
        // we use follows() to save the new following relation
        return $this->follows()->save($user);
    }

    public function following(User $user)
    {

        return $this->follows()
            ->where('following_user_id', $user->id)
            ->exists();
    }

    public function unfollow(User $user)
    {
        return $this->follows()->detach($user);
    }

    public function toggleFollow(User $user)
    {
//        if($this->following($user)){
//            return $this->unfollow($user);
//        }
//        return $this->follow($user);
        //cleaner code:
        $this->follows()->toggle($user);

    }
}
