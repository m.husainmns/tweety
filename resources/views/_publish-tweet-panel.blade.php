<div class="border border-blue-400 rounded-lg px-8 py-6 mb-6">
    <form method="POST" action="/tweets">
        @csrf
        <textarea name="body"
                  class="w-full"
                  placeholder="What's up Dock ?"
                  required
        ></textarea>

        <hr class="my-4">

        <footer class="flex justify-between items-center">
            <img src="{{auth()->user()->avatar}}"
                 alt=""
                 class="rounded-full mr-4"
                 width="40"
                 height="40"
            >
            <button type="submit"
                    class="bg-blue-400 hover:bg-blue-700 rounded-lg shadow py-1 px-2 text-white text-sm"
            >
                Tweet-a-roo!
            </button>
        </footer>

    </form>

    @error('body')
    <p class="text-sm text-red-500 mt-2">{{$message}}</p>
    @enderror
</div>
