<div class="bg-blue-200 border border-gray-300 rounded-lg py-2 px-4">
    <h3 class="font-bold text-xl mb-4">Following</h3>
    <ul>
        @forelse(auth()->user()->follows as $user)
            <li class="{{ $loop->last ? '' : 'mb-4' }}">
                <div>
                    <a class="flex items-center text-sm" href="{{route('profile', $user)}}">
                        <img src="{{$user->avatar}}"
                             alt=""
                             class="rounded-full mr-4"
                             width="50"
                             height="50"
                        >
                        {{$user->name}}
                    </a>
                </div>
            </li>
        @empty
            <p>No friends yet!</p>
        @endforelse
    </ul>
</div>
