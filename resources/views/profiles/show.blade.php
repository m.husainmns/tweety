<x-app>


    <header class="mb-6" style="position: relative">
        <div class="relative">
            <img src="/images/default-profile-banner.jpg"
                 class="mb-2"
                 alt="banner"
            >

            <img src="{{$user->avatar}}"
                 alt=""
                 class="rounded-full mr-2 absolute bottom-0 transform -translate-x-1/2 translate-y-1/2"
                 style=" left: 50%"
                 width="150"
            >
        </div>
        <div class="flex justify-between items-center mb-6">
            <div style="max-width: 270px">
                <h2 class="font-bold text-2xl">{{$user->name}}</h2>
                <p class="text-sm text-muted">Joined {{$user->created_at->diffForHumans()}}</p>
            </div>

            @can('edit', $user)
                <div class="flex">
                    <a href="{{ $user->path('edit') }}"
                       class="rounded-full border border-gray-300 mr-2 py-1 px-2 text-black text-xs"
                    >
                        Edit Profile
                    </a>
                </div>
            @endcan

            <x-follow-button :user="$user"></x-follow-button>

        </div>


        <p class="text-sm">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
            ea commodo consequat.
        </p>

    </header>

    @include('_timeline', [
        'tweets' => $tweets
    ])

</x-app>
