<x-master>
    <div class="container flex mx-auto justify-center">
        <div class="px-16 py-8 bg-blue-200 rounded-lg border border-gray-300">
            <div class="col-md-8">

                <div class="font-bold text-lg mb-4">{{ __('Login') }}</div>


                <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="mb-6">
                        <label for="email"
                               class="block mb-2 uppercase font-bold text-xs text-gray-700">
                            {{ __('E-Mail Address') }}
                        </label>

                        <input id="email"
                               type="email"
                               class="border border-gray-400 p-2 w-full"
                               name="email"
                               value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                        <strong class="text-red-500 mt-2">{{ $message }}</strong>
                        @enderror
                    </div>

                    <div class="mb-6">
                        <label for="password"
                               class="block mb-2 uppercase font-bold text-xs text-gray-700">
                            {{ __('Password') }}
                        </label>

                        <input id="password"
                               type="password"
                               class="border border-gray-400 p-2 w-full"
                               name="password"
                               value="{{ old('password') }}" required autocomplete="current-password" autofocus>

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>


                    <div class="mb-6">
                        <div>
                            <input class="mr-1" type="checkbox" name="remember"
                                   id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="text-xs text-gray-700 font-bold uppercase"
                                   for="remember"
                            >
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                        @error('remember')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>

                    <div>
                        <button type="submit"
                                class="bg-blue-400 text-white rounded py-2 px-4 hover:bg-blue-500">
                            {{ __('Login') }}
                        </button>

                        @if (Route::has('password.request'))
                            <a class="px-2 text-gray-700 text-xs hover:underline"
                               href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                    </div>

                </form>


            </div>
        </div>
    </div>
</x-master>

